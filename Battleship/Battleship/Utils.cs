﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Battleship
{
    public static class Utils
    {
        public static int[] ShipSizes = { 5, 4, 3, 3, 2 };
        public static void DrawRectangle(Rectangle rect, Color color, SpriteBatch spriteBatch)
        {
            if (rect.Width == 0 || rect.Height == 0)
                return;
            Texture2D texture = new Texture2D(spriteBatch.GraphicsDevice, rect.Width, rect.Height);
            Color[] data = new Color[texture.Width * texture.Height];
            for (int i = 0; i < data.Length; ++i)
                data[i] = color;
            texture.SetData(data);

            spriteBatch.Begin();
            spriteBatch.Draw(texture, new Vector2(rect.X, rect.Y), Color.White);
            spriteBatch.End();
        }
        public static void SetShipTypes(List<Ship> ships)
        {
            Ship.Type seen3 = Ship.Type.None;
            foreach (Ship ship in ships)
            {
                switch (ship.Size)
                {
                    case 5:
                        ship.type = Ship.Type.Carrier;
                        break;
                    case 4:
                        ship.type = Ship.Type.Battleship;
                        break;
                    case 3:
                        if (seen3 == Ship.Type.None)
                            ship.type = seen3 = Ship.Type.Destroyer;
                        else
                            ship.type = Ship.Type.Sub;
                        break;
                    case 2:
                        ship.type = Ship.Type.PatrolBoat;
                        break;
                }
            }
        }
    }
}
