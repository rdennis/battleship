using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Battleship
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Ship : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public enum Type { PatrolBoat, Destroyer, Sub, Battleship, Carrier, None }
        public enum Orientation { VERTICAL, HORIZONTAL }
        public Type type;
        public Orientation orientation;
        int size;
        public int Size { get { return size; } }
        private bool sunk;
        public bool Sunk { get { return sunk; } }
        private Rectangle boundingBox;
        List<BoardPosition> positions;
        Model model;

        public Ship(int size, Position position, Orientation orientation, Model model, Game game)
            : base(game)
        {
            this.orientation = orientation;
            this.size = size;
            this.model = model;
            this.type = Type.None;
            positions = new List<BoardPosition>();

            SetPositions(position);
        }

        public Ship(Ship other) : base(other.Game)
        {
            this.orientation = other.orientation;
            this.size = other.size;
            this.sunk = other.sunk;
            this.boundingBox = other.boundingBox;
            this.positions = other.positions;
            this.model = other.model;
            this.type = other.type;
        }

        public void SetPositions(Position position)
        {
            if (position != null)
            {
                if (orientation == Orientation.VERTICAL && position.row + size > 10 ||
                    orientation == Orientation.HORIZONTAL && position.column - 'A' + size > 10)
                    throw new IndexOutOfRangeException("BoardPosition out of bounds");
                if (positions == null)
                    positions = new List<BoardPosition>();
                else
                    positions.Clear();
                positions.Add(new BoardPosition(position.column, position.row, true));
                for (int i = 1; i < size; ++i) // add the other BoardPositions
                {
                    if (orientation == Orientation.VERTICAL)
                        positions.Add(new BoardPosition(position.column, position.row + i, true));
                    else
                        positions.Add(new BoardPosition((char)(position.column + i), position.row, true));
                }
            }
        }

        public List<Position> GetPositions()
        {
            List<Position> ret = new List<Position>();
            foreach (BoardPosition p in positions)
                ret.Add(p.position);
            return ret;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }

        public bool Contains(Point clickPos)
        {
            if (boundingBox != null)
                return boundingBox.Contains(clickPos);

            return false;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        public void Draw(Rectangle template, SpriteBatch spriteBatch, GameTime gameTime)
        {
            boundingBox = new Rectangle(template.X, template.Y, template.Width, template.Height);
            if (orientation == Orientation.HORIZONTAL)
                boundingBox.Width = (template.Width + 1) * size - 2;
            else
                boundingBox.Height = (template.Height + 1) * size - 2;

            // Utils.DrawRectangle(boundingBox, Color.Red, spriteBatch);

            for (int i = 0; i < size; ++i)
            {
                Utils.DrawRectangle(template, Color.Green, spriteBatch);
                if (orientation == Orientation.HORIZONTAL)
                    template.X += template.Width + 1;
                else
                    template.Y += template.Width + 1;
            }

            base.Draw(gameTime);
        }

        /// <summary>
        /// Attack this Ship
        /// </summary>
        /// <param name="pos">BoardPosition attacking (may or may not hit the ship)</param>
        public void Attack(Position pos)
        {
            int timesHit = 0;
            bool currentHit = false;
            foreach (BoardPosition p in positions)
            {
                if (pos == p.position)
                {
                    currentHit = true;
                    p.Attack();
                }
                if (p.attacked) // check how many times we've been hit
                    ++timesHit;
            }

            if (timesHit >= size)
            {
                sunk = true;
                if (currentHit)
                    Console.WriteLine("A {0} was sunk", size);
            }
        }
    }
}
