using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;

namespace Battleship
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        MouseState prevMouseState;
        KeyboardState prevKeyboardState;

        public enum GameState { NOGAME, PLAYER1START, PLAYER2START, PLAYER1ATTACK, PLAYER2ATTACK, PLAYER1WIN, PLAYER2WIN }
        private GameState currentState = GameState.NOGAME;
        private GameState prevGameState;
        public GameState getGameState
        {
            get { return currentState; }
        }

        bool mainMenu = true;

        public bool MainMenu { get { return mainMenu; } }

        SpriteFont titleFont;
        SpriteFont headerFont;
        SpriteFont normalFont;

        string mainMenuTitle = "Main Menu";
        string[] mainMenuHeaders, mainMenuTexts;
        Vector2 textLocation;

        string playerChooseStart = "Place your pieces";
        string playerAttack = "Your attack";
        string playerSunkShip = "You sunk a ship!";

        string playerWin = "You win!";
        string playerLose = "Better luck next time";

        Player player1, player2;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.AllowUserResizing = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            player1 = new HumanPlayer(Player.PlayerNumber.PLAYER1, this);
            player2 = new AIPlayer(Player.PlayerNumber.PLAYER2, this);

            Components.Add(player1);
            Components.Add(player2);

            mainMenuHeaders = new string[] { "Escape", "SpaceBar", "R" };
            mainMenuTexts = new string[] { "-Exit the application", "-Show/Hide this menu", "-Restart game" };

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            titleFont = Content.Load<SpriteFont>("TitleFont");
            headerFont = Content.Load<SpriteFont>("HeaderFont");
            normalFont = Content.Load<SpriteFont>("NormalFont");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Moves the game to the next state
        /// </summary>
        private void ProgressGame()
        {
            prevGameState = currentState;

            switch (currentState)
            {
                case GameState.NOGAME:
                    Console.WriteLine("Starting game");
                    currentState = GameState.PLAYER1START;
                    break;
                case GameState.PLAYER1START:
                    Console.WriteLine("Player1 choose start");
                    currentState = GameState.PLAYER2START;
                    break;
                case GameState.PLAYER2START:
                    Console.WriteLine("Player2 choose start");
                    currentState = GameState.PLAYER1ATTACK;
                    break;
                case GameState.PLAYER1ATTACK:
                    Console.WriteLine("Player1 attack");
                    currentState = (player2.ShipsAlive() > 0) ? GameState.PLAYER2ATTACK : GameState.PLAYER1WIN;
                    break;
                case GameState.PLAYER2ATTACK:
                    Console.WriteLine("Player2 attack");
                    currentState = (player1.ShipsAlive() > 0) ? GameState.PLAYER1ATTACK : GameState.PLAYER2WIN;
                    break;
                default:
                    Console.WriteLine("Game over Player{0} is the winner", (currentState == GameState.PLAYER1WIN ? 1 : 2));
                    currentState = GameState.NOGAME;
                    break;
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState currKeyboardState = Keyboard.GetState();
            MouseState currMouseState = Mouse.GetState();

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || currKeyboardState.IsKeyDown(Keys.Escape))
                this.Exit();

            IsMouseVisible = true;

            if ((prevKeyboardState == null || prevKeyboardState.IsKeyUp(Keys.Space)) && currKeyboardState.IsKeyDown(Keys.Space))
                mainMenu = !mainMenu;

            if (prevGameState != currentState)
            {
                prevGameState = currentState;

                switch (currentState)
                {
                    case GameState.NOGAME:
                        Console.WriteLine("Starting game");
                        break;
                    case GameState.PLAYER1START:
                        Console.WriteLine("Player1 choose start");
                        break;
                    case GameState.PLAYER2START:
                        Console.WriteLine("Player2 choose start");
                        break;
                    case GameState.PLAYER1ATTACK:
                        Console.WriteLine("Player1 attack");
                        break;
                    case GameState.PLAYER2ATTACK:
                        Console.WriteLine("Player2 attack");
                        break;
                    default:
                        Console.WriteLine("Game over Player{0} is the winner", (currentState == GameState.PLAYER1WIN ? 1 : 2));
                        break;
                }
            }

            if (mainMenu)
            {
                if ((prevKeyboardState == null || prevKeyboardState.IsKeyDown(Keys.R)) && prevKeyboardState.IsKeyUp(Keys.R))
                    currentState = GameState.NOGAME;
            }
            else
            {
                switch (currentState)
                {
                    case GameState.PLAYER1START:
                        if (prevGameState != GameState.PLAYER1START)
                            player1.board = new Board();
                        player1.GetStartingBoard();
                        if (player1.board.Valid())
                            ProgressGame();
                        break;
                    case GameState.PLAYER2START:
                        if (prevGameState != GameState.PLAYER2START)
                            player2.board = new Board();
                        player2.GetStartingBoard();
                        if (player2.board.Valid())
                            ProgressGame();
                        break;
                    case GameState.PLAYER1ATTACK:
                        player1.GetAttack(player2.board);
                        if (player2.ShipsAlive() < 1 || player1.attackPos != null)
                        {
                            if (player1.attackPos != null)
                            {
                                int prevShipsAlive = player2.ShipsAlive();
                                player1.lastShotHit = player2.Attack(player1.attackPos);
                                player1.justSunkShip = player2.ShipsAlive() < prevShipsAlive;
                            }

                            if (player2.ShipsAlive() < 1 || !player1.lastShotHit)
                                ProgressGame();
                            player1.attackPos = null;
                        }
                        break;
                    case GameState.PLAYER2ATTACK:
                        player2.GetAttack(player1.board);
                        if (player1.ShipsAlive() < 1 || player2.attackPos != null)
                        {
                            if (player2.attackPos != null)
                            {
                                int prevShipsAlive = player1.ShipsAlive();
                                player2.lastShotHit = player1.Attack(player2.attackPos);
                                player2.justSunkShip = player1.ShipsAlive() < prevShipsAlive;
                            }

                            if (player1.ShipsAlive() < 1 || !player2.lastShotHit)
                                ProgressGame();
                            player2.attackPos = null;
                        }
                        break;
                    default:
                        // the game is over
                        if (prevKeyboardState.IsKeyDown(Keys.R) && currKeyboardState.IsKeyUp(Keys.R))
                            ProgressGame();
                        break;
                }
            }


            prevMouseState = currMouseState;
            prevKeyboardState = currKeyboardState;

            base.Update(gameTime);
        }

        Vector2 DrawHeaderText(int index, Vector2 location)
        {
            Vector2 headerLocation = new Vector2(location.X, location.Y);
            headerLocation.Y += headerFont.MeasureString(mainMenuHeaders[index]).Y;
            headerLocation.X -= headerFont.MeasureString(mainMenuHeaders[index]).X;

            Vector2 descLocation = new Vector2(headerLocation.X, headerLocation.Y);
            descLocation.X += headerFont.MeasureString(mainMenuHeaders[index]).X;
            descLocation.Y += (headerFont.MeasureString(mainMenuHeaders[index]).Y - normalFont.MeasureString(mainMenuTexts[index]).Y) / 2;

            spriteBatch.Begin();
            spriteBatch.DrawString(headerFont, mainMenuHeaders[index], headerLocation, Color.White);
            spriteBatch.DrawString(normalFont, mainMenuTexts[index], descLocation, Color.White);
            spriteBatch.End();

            return headerLocation;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            textLocation = new Vector2((GraphicsDevice.Viewport.Width / 2), 0);

            if (mainMenu)
            {
                textLocation.X -= titleFont.MeasureString(mainMenuTitle).X / 2;
                textLocation.Y = GraphicsDevice.Viewport.Height / 3;

                spriteBatch.Begin();
                spriteBatch.DrawString(titleFont, mainMenuTitle, textLocation, Color.White);
                spriteBatch.End();

                textLocation.X = GraphicsDevice.Viewport.Width / 2;
                textLocation.Y += 10;
                for (int i = 0; i < mainMenuHeaders.Length; ++i)
                {
                    textLocation = DrawHeaderText(i, textLocation);
                    textLocation.X = GraphicsDevice.Viewport.Width / 2;
                }
            }
            else
            {
                textLocation.X -= titleFont.MeasureString("Battleship").X / 2;
                spriteBatch.Begin();
                spriteBatch.DrawString(titleFont, "Battleship", textLocation, Color.White);
                spriteBatch.End();

                string playerMessage = null;

                if (player1.GetType() == typeof(HumanPlayer) && player1.ChoosingStart() || player2.GetType() == typeof(HumanPlayer) && player2.ChoosingStart())
                    playerMessage = playerChooseStart;
                else if (player1.GetType() == typeof(HumanPlayer) && player1.Attacking() || player2.GetType() == typeof(HumanPlayer) && player2.Attacking())
                {
                    if (player1.justSunkShip || player2.justSunkShip)
                        playerMessage = playerSunkShip;
                    else
                        playerMessage = playerAttack;
                }

                if (playerMessage != null)
                {
                    textLocation.X = GraphicsDevice.Viewport.Width / 2 - headerFont.MeasureString(playerMessage).X / 2;
                    textLocation.Y += titleFont.MeasureString("Battleship").Y;
                    spriteBatch.Begin();
                    spriteBatch.DrawString(headerFont, playerMessage, textLocation, Color.White);
                    spriteBatch.End();
                }

                if (currentState == GameState.NOGAME || currentState == GameState.PLAYER1WIN || currentState == GameState.PLAYER2WIN)
                {
                    textLocation.X = GraphicsDevice.Viewport.Width / 2;
                    int winner = 0;
                    if (currentState == GameState.PLAYER1WIN)
                        winner = 1;
                    else if (currentState == GameState.PLAYER2WIN)
                        winner = 2;

                    Vector2 tmp;

                    if (winner > 0)
                    {
                        if (player1.GetType() == typeof(HumanPlayer) && winner == 1)
                            playerMessage = playerWin;
                        else
                            playerMessage = playerLose;
                        tmp = headerFont.MeasureString(playerMessage);
                        textLocation.X -= tmp.X / 2;
                        textLocation.Y += tmp.Y;
                        spriteBatch.Begin();
                        spriteBatch.DrawString(headerFont, playerMessage, textLocation, Color.White);
                        spriteBatch.End();
                    }

                    StringBuilder sb = new StringBuilder("Press R to ");

                    if (prevGameState == GameState.NOGAME)
                        sb.Append("start");
                    else
                        sb.Append("restart");

                    tmp = normalFont.MeasureString(sb.ToString());
                    textLocation.X = GraphicsDevice.Viewport.Width / 2 - tmp.X / 2;
                    textLocation.Y += tmp.Y + 20;

                    spriteBatch.Begin();
                    spriteBatch.DrawString(normalFont, sb.ToString(), textLocation, Color.White);
                    spriteBatch.End();
                }

                base.Draw(gameTime);
            }
        }

        #region Game loop
        /*
        int PlayGame(Player player1, Player player2)
        {
            player1.GetSetup();
            player2.GetSetup();

            Player attacker = player1, defender = player2, swap;
            do
            {
                BoardPosition attack;
                do
                {
                    attack = attacker.GetAttack();
                } while (!defender.AttackPosition(attack));

                // swap attaker
                swap = attacker;
                attacker = defender;
                defender = swap;
            } while (player1.Alive && player2.Alive); // game over

            if (player1.Alive)
                return 1;
            if (player2.Alive)
                return 2;
            return 0; // game ended without a winner
        }
         * */
        #endregion
    }
}
