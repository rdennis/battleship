﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Battleship
{
    class HumanPlayer : Player
    {
        private MouseState prevMouseState;
        private KeyboardState prevKeyState;
        private Position startClickPos;
        private Board targetBoard;
        private Ship carryShip;
        private List<Ship> sideShips;

        public HumanPlayer(PlayerNumber playerNumber, Game1 game) : base(playerNumber, game) 
        {
            sideShips = new List<Ship>();
            foreach (int size in Utils.ShipSizes)
                sideShips.Add(new Ship(size, null, Ship.Orientation.HORIZONTAL, null, parentGame));
            Utils.SetShipTypes(sideShips);
        }

        public override void GetStartingBoard()
        {
            // comment out for human choosing
            /*
            AIPlayer aip = new AIPlayer(PlayerNumber.PLAYER1, parentGame);
            aip.GetStartingBoard();
            this.board = new Board();
            this.Ships = aip.Ships;
             */
        }

        public override void GetAttack(Board targetBoard)
        {
            this.targetBoard = targetBoard;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            MouseState currMouseState = Mouse.GetState();
            KeyboardState currKeyState = Keyboard.GetState();

            if (IsMyTurn())
            {
                #region ship placement
                if (sideShips != null && ChoosingStart() && (prevMouseState == null || prevMouseState.LeftButton == ButtonState.Released) && currMouseState.LeftButton == ButtonState.Pressed)
                {
                    foreach (Ship ship in sideShips)
                    {
                        if (ship.Contains(new Point(currMouseState.X, currMouseState.Y)))
                        {
                            bool alreadyUsed = false;
                            if (Ships != null)
                            {
                                foreach (Ship s in Ships)
                                    if (s.type == ship.type)
                                        alreadyUsed = true;
                            }
                            if (!alreadyUsed)
                            {
                                Console.WriteLine("Picked up a {0}", ship.Size);
                                carryShip = new Ship(ship);
                            }
                        }
                    }
                }

                if (carryShip != null && ChoosingStart() && (prevMouseState == null || prevMouseState.LeftButton == ButtonState.Released) && currMouseState.LeftButton == ButtonState.Pressed)
                {
                    BoardPosition bp = board.MapPoint(new Point(currMouseState.X, currMouseState.Y));
                    if (bp != null)
                    {
                        try
                        {
                            List<Ship> tmpShips = ships == null ? new List<Ship>() : new List<Ship>(ships);
                            Ship newShip = new Ship(carryShip);
                            newShip.SetPositions(bp.position);
                            tmpShips.Add(newShip);
                            Ships = tmpShips;
                            carryShip = null;
                        }
                        catch (IndexOutOfRangeException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }

                if (carryShip != null && (prevKeyState == null || prevKeyState.IsKeyUp(Keys.Tab)) && currKeyState.IsKeyDown(Keys.Tab))
                    carryShip.orientation = carryShip.orientation == Ship.Orientation.HORIZONTAL ? Ship.Orientation.VERTICAL : Ship.Orientation.HORIZONTAL;
                #endregion

                if (targetBoard != null && startClickPos == null && currMouseState.LeftButton == ButtonState.Pressed)
                {
                    BoardPosition bp = targetBoard.MapPoint(new Point(currMouseState.X, currMouseState.Y));
                    if(bp != null)
                        startClickPos = bp.position;
                }

                if (prevMouseState != null && startClickPos != null && prevMouseState.LeftButton == ButtonState.Pressed && currMouseState.LeftButton == ButtonState.Released)
                {
                    BoardPosition attack = targetBoard.MapPoint(new Point(currMouseState.X, currMouseState.Y));
                    if (attack != null && startClickPos == attack.position && !attack.attacked)
                        attackPos = attack.position;
                }
            }

            if (!IsMyTurn() || currMouseState.LeftButton == ButtonState.Released)
                startClickPos = null;

            prevMouseState = currMouseState;
            prevKeyState = currKeyState;

            base.Update(gameTime);
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            int drawSize = (int)(parentGame.GraphicsDevice.Viewport.Width / 3);
            int horizontalBorder = 10;// (parentGame.GraphicsDevice.Viewport.Width - drawSize * 2) / 3; // left, middle and right borders
            int verticalBorder = (parentGame.GraphicsDevice.Viewport.Height - drawSize) / 2;

            Rectangle drawRect = new Rectangle(horizontalBorder, verticalBorder, drawSize, drawSize);
            SpriteBatch spriteBatch = new SpriteBatch(parentGame.GraphicsDevice);

            if (targetBoard != null)
                targetBoard.Draw(drawRect, spriteBatch);
            else
                new Board().Draw(drawRect, spriteBatch);

            drawRect.X += drawRect.Width + horizontalBorder;

            if (board != null)
                board.Draw(drawRect, spriteBatch, true);
            else
                new Board().Draw(drawRect, spriteBatch);

            #region draw health bar
            Rectangle healthRect = drawRect;
            healthRect.Height = 15;
            healthRect.Width -= horizontalBorder / 2;
            healthRect.X += horizontalBorder / 4;
            healthRect.Y -= healthRect.Height;

            Utils.DrawRectangle(healthRect, Color.Gray, spriteBatch);

            double damage = DamagePercentage();
            healthRect.Width = (int)(healthRect.Width * damage);

            Color color;

            if (damage >= 0.8)
                color = Color.Red;
            else if (damage >= 0.5)
                color = Color.Yellow;
            else
                color = Color.Green;

            Utils.DrawRectangle(healthRect, color, spriteBatch);
            #endregion

            #region draw ships
            if (sideShips != null)
            {
                int cellWidth = (GraphicsDevice.Viewport.Width - (drawRect.X + drawRect.Width + 30)) / 6;
                int origCellX = drawRect.X + drawRect.Width + 35;
                int origCellY = drawRect.Y + (drawRect.Height - ((cellWidth + 10) * 5)) / 2; // / 10;
                Rectangle cellRect = new Rectangle(origCellX, origCellY, cellWidth, cellWidth);

                foreach (Ship ship in sideShips)
                {
                    cellRect.X = origCellX;
                    ship.Draw(cellRect, spriteBatch, gameTime);
                    cellRect.Y += cellRect.Height + 10;
                }

                if (carryShip != null)
                {
                    MouseState currMouseState = Mouse.GetState();
                    Rectangle carryRect = new Rectangle(currMouseState.X, currMouseState.Y, drawRect.Width / 10, drawRect.Height / 10);
                    carryRect.X = currMouseState.X - carryRect.Width / 2;
                    carryRect.Y = currMouseState.Y - carryRect.Height / 2;
                    carryShip.Draw(carryRect, spriteBatch, gameTime);
                }
            }
            #endregion

            base.Draw(gameTime);
        }
    }
}
