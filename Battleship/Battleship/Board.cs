﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Battleship
{
    class Board
    {
        BoardPosition[,] board;

        private Rectangle drawRect;
        private int cellPadding;

        public Board()
        {
            board = new BoardPosition[10, 10];

            for (char col = 'A'; col < 'A' + 10; ++col)
                for (int row = 1; row <= 10; ++row)
                    board[col - 'A', row - 1] = new BoardPosition(col, row);
        }

        /// <summary>
        /// Get the BoardPosition at the given Position
        /// </summary>
        /// <param name="position">The Position to get</param>
        /// <returns>The BoardPosition at position</returns>
        public BoardPosition this[Position position]
        {
            get
            {
                return board[position.column - 'A', position.row - 1];
            }
            set
            {
                board[position.column - 'A', position.row - 1] = value;
            }
        }
        public BoardPosition this[char column, int row]
        {
            get
            {
                return board[column - 'A', row - 1];
            }
            set
            {
                board[column - 'A', row - 1] = value;
            }
        }

        public bool Valid()
        {
            int occupiedSpaces = 0;
            foreach (BoardPosition position in board)
                if (position.occupied)
                    ++occupiedSpaces;
            return occupiedSpaces == Utils.ShipSizes.Sum();
        }

        /// <summary>
        /// Attack a position on the Board
        /// </summary>
        /// <param name="pos">The position to attack</param>
        /// <returns>true if a hit on pos or it has been attackd before; false otherwise</returns>
        public bool AttackPosition(Position pos)
        {
            return this[pos.column, pos.row].Attack();
        }

        public BoardPosition MapPoint(Point p)
        {
            Point relativePoint = new Point(p.X - drawRect.X, p.Y - drawRect.Y);

            if (relativePoint.X < 0 || relativePoint.X > drawRect.Width || relativePoint.Y < 0 || relativePoint.Y > drawRect.Height)
                return null;

            int cellHeight = (drawRect.Height / 10);
            int cellWidth = (drawRect.Width / 10);

            char col = (char)((relativePoint.X / cellWidth) + 'A');
            int row = (relativePoint.Y / cellHeight) + 1;

            if(col >= 'A' && col < 'A' + 10 && row > 0 && row < 11)
                return this[col, row];
            return null;
        }

        public void Draw(Rectangle drawRect, SpriteBatch spriteBatch, bool showShips = false, int cellPadding = 2)
        {
            this.drawRect = drawRect;
            this.cellPadding = cellPadding;

            int cellHeight = (drawRect.Height / 10) - (cellPadding);
            int cellWidth =  (drawRect.Width / 10)  - (cellPadding);

            //DrawRectangle(drawRect, Color.Thistle, spriteBatch);

            foreach (BoardPosition position in board)
            {
                Color color;
                int col = position.position.column - 'A';
                int row = position.position.row - 1;
                int x = drawRect.X + (cellWidth * col) + (cellPadding * col) + (cellPadding * 2);
                int y = drawRect.Y + (cellHeight * row) + (cellPadding * row) + (cellPadding * 2);

                if (position.occupied && position.attacked)
                    color = Color.DarkRed;
                else if (position.occupied && showShips)
                    color = Color.Green;
                else if (position.attacked)
                    color = Color.White;
                else
                    color = Color.Gray;

                Utils.DrawRectangle(new Rectangle(x, y, cellWidth, cellHeight), color, spriteBatch);
            }
        }
    }
}
