﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Battleship
{
    public class Position
    {
        public char column;
        public int row;

        public Position() { }

        public Position(char column, int row)
        {
            this.column = column; this.row = row;
        }

        public static bool operator ==(Position p1, Position p2)
        {
            if (ReferenceEquals(p1, p2))
                return true;
            if ((object)p1 == null || (object)p2 == null)
                return false;
            return p1.column == p2.column && p1.row == p2.row;
        }

        public static bool operator !=(Position p1, Position p2)
        {
            return !(p1 == p2);
        }
    }
}
