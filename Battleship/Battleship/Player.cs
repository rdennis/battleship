﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Battleship
{
    abstract class Player : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public bool justSunkShip = false, lastShotHit = false;

        public enum PlayerNumber { PLAYER1, PLAYER2 };
        protected PlayerNumber playerNumber;
        protected List<Ship> ships;
        protected Game1 parentGame;

        public Board board;
        public Position attackPos;

        public Player(PlayerNumber playerNumber, Game1 game)
            : base(game)
        {
            this.board = new Board();
            this.parentGame = game;
            this.playerNumber = playerNumber;
        }

        public bool Attacking()
        {
            return IsMyTurn() && (parentGame.getGameState == Game1.GameState.PLAYER1ATTACK || parentGame.getGameState == Game1.GameState.PLAYER2ATTACK);
        }

        public bool ChoosingStart()
        {
            return IsMyTurn() && (parentGame.getGameState == Game1.GameState.PLAYER1START || parentGame.getGameState == Game1.GameState.PLAYER2START);
        }

        public bool IsMyTurn()
        {
            if (parentGame.MainMenu)
                return false;
            if (playerNumber == PlayerNumber.PLAYER1 && (
                parentGame.getGameState == Game1.GameState.PLAYER1START ||
                parentGame.getGameState == Game1.GameState.PLAYER1ATTACK))
                return true;
            if (playerNumber == PlayerNumber.PLAYER2 && (
                parentGame.getGameState == Game1.GameState.PLAYER2START ||
                parentGame.getGameState == Game1.GameState.PLAYER2ATTACK))
                return true;

            return false;
        }

        /// <summary>
        /// Set the Player's ships and change the board to reflect the Ship locations
        /// </summary>
        /// <param name="ships">List of Ships to set as the Player's Ships</param>
        public List<Ship> Ships
        {
            get { return this.ships; }
            set
            {
                Board tmpBoard = new Board();
                foreach (Ship ship in value)
                {
                    foreach (Position pos in ship.GetPositions())
                    {
                        if (tmpBoard[pos].occupied)
                        {
                            StringBuilder sb = new StringBuilder();
                            throw new ArgumentException(sb.AppendFormat("Ships won't fit on board, {0},{1} occupied", tmpBoard[pos].position.column, tmpBoard[pos].position.row).ToString());
                        }
                        tmpBoard[pos].occupied = true;
                    }
                }
                this.board = tmpBoard;
                this.ships = value;

                Utils.SetShipTypes(ships);
            }
        }

        /// <summary>
        /// Tell how many ships are not sunk
        /// </summary>
        /// <returns>The number of ships not sunk</returns>
        public int ShipsAlive()
        {
            int living = 0;
            foreach (Ship ship in ships)
                if (!ship.Sunk)
                    ++living;
            return living;
        }

        public double DamagePercentage()
        {
            double health = 0, damage = 0;

            if (Ships != null)
            {
                foreach (Ship ship in Ships)
                    foreach (Position position in ship.GetPositions())
                    {
                        ++health;
                        if (board[position].attacked)
                            ++damage;
                    }
            }
            return health == 0 ? 0 : damage / health;
        }

        abstract public void GetStartingBoard();
        abstract public void GetAttack(Board targetBoard);

        public bool Attack(Position position)
        {
            foreach (Ship ship in Ships)
                ship.Attack(position);
            return board.AttackPosition(position);
        }
    }
}
