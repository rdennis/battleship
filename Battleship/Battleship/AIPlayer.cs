﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Battleship
{
    class AIPlayer : Player
    {
        int attackCountDown = 60, attackTimeout = 60;
        Position lastAttack;

        public AIPlayer(Player.PlayerNumber playerNumber, Game1 game) : base(playerNumber, game)
        {
            ships = new List<Ship>();
            board = new Board();
            GetStartingBoard();
        }

        private bool ValidSpot(int size, Position position, Ship.Orientation orientation)
        {
            if (orientation == Ship.Orientation.VERTICAL)
            {
                if (position.row + size > 10)
                    return false;

                for (int row = position.row; row < position.row + size; ++row)
                    if (board[position.column, row].occupied)
                        return false;
            }
            else
            {
                if (position.column + size > 'A' + 10)
                    return false;

                for (char col = position.column; col < position.column + size; ++col)
                    if (board[col, position.row].occupied)
                        return false;
            }

            return true;
        }

        public override void GetStartingBoard()
        {
            if (ships == null)
                ships = new List<Ship>();
            else
                ships.Clear();

            Stack<int> shipSizes = new Stack<int>(Utils.ShipSizes);

            Random rand = new Random();

            while (shipSizes.Count > 0)
            {
                int size = shipSizes.Pop();
                Position pos = new Position();
                Ship.Orientation orientation;
                do
                {
                    orientation = rand.Next() % 2 == 0 ? Ship.Orientation.VERTICAL : Ship.Orientation.HORIZONTAL;
                    pos.row = rand.Next(1, 11 - (orientation == Ship.Orientation.VERTICAL ? size : 0));
                    pos.column = (char)(rand.Next('A', 'A' + 10 - (orientation == Ship.Orientation.HORIZONTAL ? size : 0)));
                } while (!ValidSpot(size, pos, orientation));

                ships.Add(new Ship(size, pos, orientation, null, null));
                Ships = ships;
            }
        }

        public override void GetAttack(Board targetBoard)
        {
            if (--attackCountDown > 0)
                return;
            Random randomCount = new Random();
            attackCountDown = randomCount.Next(attackTimeout / 2, attackTimeout + 1);

            Random rand = new Random();
            attackPos = new Position();
            do
            {
                if (lastShotHit && !justSunkShip)
                {
                    attackPos = lastAttack;
                    switch (rand.Next(4))
                    {
                        case 0:
                            if (lastAttack.row > 1)
                                --attackPos.row;
                            break;
                        case 1:
                            if (lastAttack.row < 10)
                                ++attackPos.row;
                            break;
                        case 2:
                            if (lastAttack.column > 'A')
                                --attackPos.column;
                            break;
                        default:
                            if (lastAttack.column < 'A' + 9)
                                ++attackPos.column;
                            break;
                    }
                }
                else
                {
                    attackPos.row = rand.Next(1, 11);
                    attackPos.column = (char)(rand.Next('A', 'A' + 10));
                }
            } while (targetBoard[attackPos].attacked); // find a position that hasn't been attacked

            lastAttack = attackPos;
        }
    }
}
