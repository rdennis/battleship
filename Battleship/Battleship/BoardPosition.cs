﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Battleship
{
    class BoardPosition
    {
        public Position position;
        public bool occupied, attacked;

        public BoardPosition(char column, int row, bool occupied = false, bool attacked = false)
        {
            this.position = new Position(column, row);
            this.occupied = occupied;
            this.attacked = attacked;
        }

        public bool Attack()
        {
            attacked = true;
            if (occupied)
                return true;
            return false;
        }
    }
}
